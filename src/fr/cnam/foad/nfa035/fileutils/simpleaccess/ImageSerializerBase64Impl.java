package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;

/**
 * Cette classe contient 2 méthodes pour sérialiser un File en Strnig et désérialiser un string en byte[]
 */



public class ImageSerializerBase64Impl implements ImageSerializer {

	@Override
	public String serialize(File image)  {

	/**
	 * Cette méthode ouvre un stream fait passer les bytes lus dans un tableau
	 * puis transforme les bytes en caractères base64 dans une String
	 * @param args une image à sérialiser
	 */
		
		String serializedImage = "a";
		
		
		 try {
			 FileInputStream imageStream = new FileInputStream(image);
			 byte[] imageByte = imageStream.readAllBytes();
			 serializedImage = Base64.getEncoder().encodeToString(imageByte);
			 imageStream.close();
			 
			 
		    } 
		 catch (IOException e) {
		        e.printStackTrace();
		 }
		 return serializedImage;
		
	}

	@Override
	public byte[] deserialize(String encodedImage) {
/**
 * Cette méthode décode une String et place le contenu dans un byte[]
 * @param args une Strng contenant le code base64 d'une image
 */
		byte[] deserializedImage = Base64.getDecoder().decode(encodedImage);
		
		return deserializedImage;
	}

}
